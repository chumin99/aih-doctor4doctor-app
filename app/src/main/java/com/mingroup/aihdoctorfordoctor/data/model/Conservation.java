package com.mingroup.aihdoctorfordoctor.data.model;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
public class Conservation {
    @NonNull
    String id;
    @NonNull
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference patient;
    @NonNull
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference doctor;
    @NonNull
    List<Message> messageList;
    Message lastMessage;
}

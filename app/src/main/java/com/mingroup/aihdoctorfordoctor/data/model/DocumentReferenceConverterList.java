package com.mingroup.aihdoctorfordoctor.data.model;

import android.os.Parcel;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.ParcelConverter;
import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class DocumentReferenceConverterList implements ParcelConverter<List<DocumentReference>> {
    @Override
    public void toParcel(List<DocumentReference> input, Parcel parcel) {
        if(input == null) {
            parcel.writeInt(-1);
        } else {
            parcel.writeInt(input.size());
            for(DocumentReference docRef : input) {
                parcel.writeParcelable(Parcels.wrap(docRef), 0);
            }
        }
    }

    @Override
    public List<DocumentReference> fromParcel(Parcel parcel) {
        int size = parcel.readInt();
        if(size < 0)
            return null;
        List<DocumentReference> list = new ArrayList<>();
        for(int i = 0; i < size; ++i) {
            list.add((DocumentReference)Parcels.unwrap(parcel.readParcelable(DocumentReference.class.getClassLoader())));
        }
        return list;
    }
}

package com.mingroup.aihdoctorfordoctor.data.model;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;
import org.w3c.dom.Document;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reply {
    String content;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference fromDoctor;
    Date createdAt;
    int love_count;
}

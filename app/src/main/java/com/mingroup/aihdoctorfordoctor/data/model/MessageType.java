package com.mingroup.aihdoctorfordoctor.data.model;

public enum MessageType {
    TEXT,
    IMAGE
}

package com.mingroup.aihdoctorfordoctor.data.model;

import com.google.firebase.auth.FirebaseUser;

import org.parceler.Parcel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    String uid;
    String displayName;
    String email;
    String phoneNumber;
    boolean isMale;
    Date dob;
    String photoUrl;
    HealthRecord healthRecord;

    public static User convertFrom(FirebaseUser firebaseUser) {
        User user = new User();
        user.setDisplayName(firebaseUser.getDisplayName());
        user.setEmail(firebaseUser.getEmail());
        user.setPhoneNumber(firebaseUser.getPhoneNumber());
        user.setPhotoUrl(firebaseUser.getPhotoUrl() == null ? null : firebaseUser.getPhotoUrl().toString());
        return user;
    }
}

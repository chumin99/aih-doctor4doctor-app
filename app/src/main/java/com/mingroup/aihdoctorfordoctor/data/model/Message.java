package com.mingroup.aihdoctorfordoctor.data.model;

import org.parceler.Parcel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    String content;
    String fromUid;
    String toUid;
    Date timestamp;
    MessageType type;
}

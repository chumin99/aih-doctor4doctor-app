package com.mingroup.aihdoctorfordoctor.data.model;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Specialty {
    String id;
    String name;
    String iconUrl;
}

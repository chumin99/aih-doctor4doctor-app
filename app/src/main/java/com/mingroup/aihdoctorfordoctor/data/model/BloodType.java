package com.mingroup.aihdoctorfordoctor.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum BloodType {
    TYPE_O_BLOOD("O"),
    TYPE_A_BLOOD("A"),
    TYPE_B_BLOOD("B"),
    TYPE_AB_BLOOD("AB");

    String bloodType;

    private static BloodType getBloodTypeByString (String bloodTypeStr){
        for (BloodType bloodType : BloodType.values()) {
            if(bloodType.bloodType.equalsIgnoreCase(bloodTypeStr))
                return bloodType;
        }
        return null;
    }
}

package com.mingroup.aihdoctorfordoctor.data.model;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {
    String id;
    String content;
    List<String> images;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference fromUser;
    Date createdAt;
    @ParcelPropertyConverter(DocumentReferenceConverter.class)
    DocumentReference specialty;
    boolean isPublic;
    Reply reply;
}

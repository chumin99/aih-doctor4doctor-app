package com.mingroup.aihdoctorfordoctor.ui.conservation.chatwindow;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.constaint.CollectionConst;
import com.mingroup.aihdoctorfordoctor.data.model.Message;
import com.mingroup.aihdoctorfordoctor.data.model.MessageType;
import com.mingroup.aihdoctorfordoctor.data.model.User;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;
import com.mingroup.aihdoctorfordoctor.util.StringUtil;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;

public class ChatListAdapter extends RecyclerView.Adapter {
    @AllArgsConstructor
    @Getter
    enum ChatItemType {
        CHAT_ITEM_RIGHT_TEXT(1),
        CHAT_ITEM_RIGHT_IMAGE(2),
        CHAT_ITEM_LEFT_TEXT(3),
        CHAT_ITEM_LEFT_IMAGE(4);

        private int typeId;

        public static ChatItemType getChatItemType(final int id) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Stream.of(ChatItemType.values())
                        .filter(targetStatus -> targetStatus.typeId == id).findFirst().get();
            } else {
                for (ChatItemType chatItemType : ChatItemType.values()) {
                    if (chatItemType.typeId == id)
                        return chatItemType;
                }
            }
            return null;
        }
    }

    private Context context;
    private List<Message> messageList;
    private OnChatItemListener onChatItemListener;
    private AsyncListDiffer<Message> mDiffer;
    private DiffUtil.ItemCallback<Message> diffCallback = new DiffUtil.ItemCallback<Message>() {
        @Override
        public boolean areItemsTheSame(@NonNull Message oldItem, @NonNull Message newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Message oldItem, @NonNull Message newItem) {
            return oldItem.getContent().equals(newItem.getContent());
        }
    };
    private FirebaseFirestore db;
    private PrettyTime prettyTime;

    public ChatListAdapter(Context context, List<Message> messageList, OnChatItemListener onChatItemListener) {
        this.context = context;
        this.messageList = messageList;
        this.onChatItemListener = onChatItemListener;
        this.mDiffer = new AsyncListDiffer<Message>(this, diffCallback);
        this.db = FirebaseFirestore.getInstance();
        this.prettyTime = new PrettyTime(ComponentUtil.getCurrentLocale(context));
    }

    @Override
    public int getItemViewType(int position) {
        Message messageItem = getItem(position);
        String currentUserUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        if (messageItem.getFromUid().equals(currentUserUid)) {
            if (messageItem.getType() == MessageType.TEXT)
                return ChatItemType.CHAT_ITEM_RIGHT_TEXT.typeId;
            else
                return ChatItemType.CHAT_ITEM_RIGHT_IMAGE.typeId;
        } else {
            if (messageItem.getType() == MessageType.TEXT)
                return ChatItemType.CHAT_ITEM_LEFT_TEXT.typeId;
            else
                return ChatItemType.CHAT_ITEM_LEFT_IMAGE.typeId;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view;
        ChatItemType chatItemType = ChatItemType.getChatItemType(viewType);
        switch (chatItemType) {
            case CHAT_ITEM_RIGHT_TEXT:
                view = layoutInflater.inflate(R.layout.item_chat_message_right_text, parent, false);
                return new TextMessageVH(view, onChatItemListener, false);
            case CHAT_ITEM_LEFT_TEXT:
                view = layoutInflater.inflate(R.layout.item_chat_message_left_text, parent, false);
                return new TextMessageVH(view, onChatItemListener, true);
            case CHAT_ITEM_RIGHT_IMAGE:
                view = layoutInflater.inflate(R.layout.item_chat_message_right_media, parent, false);
                return new ImageMessageVH(view, onChatItemListener, false);
            case CHAT_ITEM_LEFT_IMAGE:
                view = layoutInflater.inflate(R.layout.item_chat_message_left_media, parent, false);
                return new ImageMessageVH(view, onChatItemListener, true);
            default:
                throw new IllegalStateException("Unexpected value: " + viewType);
        }
    }

    public void addNewMessage(@NonNull Message chatMessage) {
        messageList.add(chatMessage);
        notifyItemInserted(messageList.size() - 1);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Message messageItem = getItem(position);
        ChatItemType chatItemType = ChatItemType.getChatItemType(getItemViewType(position));
        boolean isBreakTimestamp = false;
        boolean isSameTime = false;

        if (getItemCount() > 1 && position > 0) {
            Message previousMessage = getItem(position - 1);
            Log.d("timeDiff", position + "; " + StringUtil.getDateDiff(previousMessage.getTimestamp(), messageItem.getTimestamp(), TimeUnit.DAYS) + "");
            if (StringUtil.getDateDiff(previousMessage.getTimestamp(), messageItem.getTimestamp(), TimeUnit.DAYS) >= 1) {
                isBreakTimestamp = true;
            }
        }

        if (getItemCount() > 1 && position != (getItemCount() - 1)) {
            Message nextMessage = getItem(position + 1);
            if(StringUtil.getDateDiff(messageItem.getTimestamp(), nextMessage.getTimestamp(), TimeUnit.SECONDS) < 60) {
                isSameTime = true;
            }
        }

        switch (chatItemType) {
            case CHAT_ITEM_RIGHT_TEXT:
                displayTextMessage((TextMessageVH) holder, messageItem, false, isBreakTimestamp, isSameTime);
                break;
            case CHAT_ITEM_LEFT_TEXT:
                displayTextMessage((TextMessageVH) holder, messageItem, true, isBreakTimestamp, isSameTime);
                break;
            case CHAT_ITEM_RIGHT_IMAGE:
                displayMediaMessage((ImageMessageVH) holder, messageItem, false, isBreakTimestamp, isSameTime);
                break;
            case CHAT_ITEM_LEFT_IMAGE:
                displayMediaMessage((ImageMessageVH) holder, messageItem, true, isBreakTimestamp, isSameTime);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + chatItemType);
        }
    }

    @SneakyThrows
    private void displayTextMessage(TextMessageVH textMessageViewHolder, Message messageItem,
                                    boolean fromLeft, boolean isBreakTimestamp, boolean isSameTime) {
        textMessageViewHolder.getTvContent().setText(messageItem.getContent());
        textMessageViewHolder.getTvTimestamp().setText(StringUtil.formatChatTime(messageItem.getTimestamp()));
        if (fromLeft) {
            db.collection(CollectionConst.COLLECTION_USER)
                    .document(messageItem.getFromUid())
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            User user = documentSnapshot.toObject(User.class);
                            ComponentUtil.displayImage(textMessageViewHolder.getIvAvatar(), user.getPhotoUrl());
                            textMessageViewHolder.getTvDisplayName().setText(user.getDisplayName());
                        }
                    });
        }

        if (isBreakTimestamp) {
            textMessageViewHolder.getTvBreakTimeStamp().setVisibility(View.VISIBLE);
            Log.d("breakTimeStamp", messageItem.getTimestamp().toString());
            textMessageViewHolder.getTvBreakTimeStamp().setText(StringUtil.formatChatDateTime(messageItem.getTimestamp()));
        }

//        if(isSameTime) {
//            textMessageViewHolder.getTvTimestamp().setVisibility(View.GONE);
//        }
    }

    @SneakyThrows
    private void displayMediaMessage(ImageMessageVH mediaMessageViewHolder, Message messageItem,
                                     boolean fromLeft, boolean isBreakTimestamp, boolean isSameTime) {
        mediaMessageViewHolder.getTvTimestamp().setText(StringUtil.formatChatTime(messageItem.getTimestamp()));
        ComponentUtil.displayImage(mediaMessageViewHolder.getIvContent(), messageItem.getContent());

        if (fromLeft) {
            db.collection(CollectionConst.COLLECTION_USER)
                    .document(messageItem.getFromUid())
                    .get()
                    .addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            User user = documentSnapshot.toObject(User.class);
                            ComponentUtil.displayImage(mediaMessageViewHolder.getIvAvatar(), user.getPhotoUrl());
                            mediaMessageViewHolder.getTvDisplayName().setText(user.getDisplayName());
                        }
                    });
        }

        if (isBreakTimestamp) {
            mediaMessageViewHolder.getTvBreakTimeStamp().setVisibility(View.VISIBLE);
            mediaMessageViewHolder.getTvBreakTimeStamp().setText(StringUtil.formatChatDateTime(messageItem.getTimestamp()));
        }

        if(isSameTime) {
            mediaMessageViewHolder.getTvTimestamp().setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public Message getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void submitList(List<Message> newList) {
        mDiffer.submitList(newList);
    }

    @Getter
    public class TextMessageVH extends RecyclerView.ViewHolder {
        private RoundedImageView ivAvatar;
        private TextView tvDisplayName, tvContent, tvTimestamp, tvBreakTimeStamp;
        private OnChatItemListener onChatItemListener;

        public TextMessageVH(@NonNull View itemView, OnChatItemListener onChatItemListener, boolean fromLeft) {
            super(itemView);
            tvContent = itemView.findViewById(R.id.text_message_item_content);
            tvTimestamp = itemView.findViewById(R.id.text_message_item_timestamp);
            tvBreakTimeStamp = itemView.findViewById(R.id.break_timestamp);
            this.onChatItemListener = onChatItemListener;

            if (fromLeft) {
                ivAvatar = itemView.findViewById(R.id.text_message_item_avatar);
                tvDisplayName = itemView.findViewById(R.id.text_message_item_display_name);
            }

            itemView.setOnLongClickListener(view -> {
                if (onChatItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onChatItemListener.onChatItemLongClickListener(position);
                        return true;
                    }
                }
                return false;
            });
        }
    }

    public interface OnChatItemListener {
        void onChatItemLongClickListener(int position);

        void onChatItemClickListener(int position);
    }

    @Getter
    public class ImageMessageVH extends RecyclerView.ViewHolder {
        private RoundedImageView ivAvatar;
        private TextView tvDisplayName, tvTimestamp, tvBreakTimeStamp;
        private RoundedImageView ivContent;
        private OnChatItemListener onChatItemListener;

        public ImageMessageVH(@NonNull View itemView, OnChatItemListener onChatItemListener, boolean fromLeft) {
            super(itemView);
            tvTimestamp = itemView.findViewById(R.id.media_message_item_timestamp);
            ivContent = itemView.findViewById(R.id.media_message_item_imageview);
            tvBreakTimeStamp = itemView.findViewById(R.id.break_timestamp);
            this.onChatItemListener = onChatItemListener;

            if (fromLeft) {
                ivAvatar = itemView.findViewById(R.id.media_message_item_avatar);
                tvDisplayName = itemView.findViewById(R.id.media_message_item_display_name);
            }

            itemView.setOnClickListener(view -> {
                if (onChatItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onChatItemListener.onChatItemClickListener(position);
                    }
                }
            });

            itemView.setOnLongClickListener(view -> {
                if (onChatItemListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onChatItemListener.onChatItemLongClickListener(position);
                        return true;
                    }
                }
                return false;
            });
        }
    }
}

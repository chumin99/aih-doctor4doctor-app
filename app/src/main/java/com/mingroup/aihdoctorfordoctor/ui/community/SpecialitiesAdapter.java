package com.mingroup.aihdoctorfordoctor.ui.community;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.google.common.collect.Sets;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.data.model.Specialty;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;

public class SpecialitiesAdapter extends RecyclerView.Adapter<SpecialitiesAdapter.SpecialtyViewHolder> {
    private Context context;
    private List<Specialty> specialties;
    private OnSpecialtyListener onSpecialtyListener;
    private AsyncListDiffer<Specialty> mDiffer;
    private int lastCheckedPosition = 0;
    private Set<Integer> lastCheckedPositions = new HashSet<>();
    private boolean isSingleChoice;

    private DiffUtil.ItemCallback<Specialty> diffCallback = new DiffUtil.ItemCallback<Specialty>() {
        @Override
        public boolean areItemsTheSame(@NonNull Specialty oldItem, @NonNull Specialty newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull Specialty oldItem, @NonNull Specialty newItem) {
            return oldItem.getName().equals(newItem.getName());
        }
    };

    public SpecialitiesAdapter(Context context, List<Specialty> specialties, boolean isSingleChoice, OnSpecialtyListener onSpecialtyListener) {
        this.context = context;
        this.specialties = specialties;
        this.onSpecialtyListener = onSpecialtyListener;
        this.isSingleChoice = isSingleChoice;
        this.mDiffer = new AsyncListDiffer<>(this, diffCallback);
    }

    @NonNull
    @Override
    public SpecialtyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SpecialtyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_specialities, parent, false), onSpecialtyListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SpecialtyViewHolder holder, int position) {
        Specialty specialty = getItem(position);
        ComponentUtil.displayImage(holder.getSpecIcon(), specialty.getIconUrl());
        holder.getSpecName().setText(specialty.getName());

        if (isSingleChoice) {
            holder.getSpecItem().setChecked(position == lastCheckedPosition);
        } else {
//            if(lastCheckedPositions.size() > 1 && lastCheckedPositions.contains(0)) {
//                lastCheckedPositions.remove(0);
//            }
            if (lastCheckedPositions.size() > 0)
                holder.getSpecItem().setChecked(lastCheckedPositions.contains(position));
            else
                holder.getSpecItem().setChecked(!lastCheckedPositions.contains(position));
        }
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public void submitList(List<Specialty> newList) {
        mDiffer.submitList(newList != null ? new ArrayList<>(newList) : null);
    }

    public Specialty getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    @Getter
    public class SpecialtyViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView specIcon;
        private TextView specName;
        private MaterialCardView specItem;
        private OnSpecialtyListener onSpecialtyListener;

        public SpecialtyViewHolder(@NonNull View itemView, OnSpecialtyListener onSpecialtyListener) {
            super(itemView);
            specIcon = itemView.findViewById(R.id.specialty_icon);
            specName = itemView.findViewById(R.id.specialty_name);
            specItem = itemView.findViewById(R.id.specialty_item);
            this.onSpecialtyListener = onSpecialtyListener;

            itemView.setOnClickListener(view -> {
                if (onSpecialtyListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onSpecialtyListener.onSpecialtyClickListener(position);

                        if (isSingleChoice) {
                            int copyOfLastCheckedPosition = lastCheckedPosition;
                            lastCheckedPosition = getAdapterPosition();
                            notifyItemChanged(copyOfLastCheckedPosition);
                            notifyItemChanged(lastCheckedPosition);
                            specItem.setChecked(!specItem.isChecked());
                        } else {
                            Set<Integer> copyOfLastCheckedPositions = lastCheckedPositions;
                            if (specItem.isChecked())
                                lastCheckedPositions.add(position);
                            else
                                lastCheckedPositions.remove(position);

                            copyOfLastCheckedPositions.forEach(copyOfLastCheckedPosition -> {
                                notifyItemChanged(copyOfLastCheckedPosition);
                            });

                            lastCheckedPositions.forEach(lastCheckedPosition -> {
                                notifyItemChanged(lastCheckedPosition);
                            });
                        }

                    }

                }
            });

            specItem.setOnCheckedChangeListener(new MaterialCardView.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(MaterialCardView card, boolean isChecked) {
                    if (onSpecialtyListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            if (isChecked) {
                                specItem.setCardBackgroundColor(context.getResources().getColor(R.color.specialty_icon_tint, context.getTheme()));
                                specName.setTextColor(context.getResources().getColor(android.R.color.white, context.getTheme()));
                            } else {
                                specItem.setCardBackgroundColor(context.getResources().getColor(R.color.chat_icon_bg, context.getTheme()));
                                specName.setTextColor(context.getResources().getColor(android.R.color.black, context.getTheme()));
                            }
                            onSpecialtyListener.onSpecialtyCheckedChangeListener(card, isChecked, position);
                        }
                    }

                }
            });
        }
    }

    public interface OnSpecialtyListener {
        void onSpecialtyClickListener(int position);

        void onSpecialtyCheckedChangeListener(View view, boolean isChecked, int position);
    }
}

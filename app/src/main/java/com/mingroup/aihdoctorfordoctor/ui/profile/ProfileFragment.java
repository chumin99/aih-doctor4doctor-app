package com.mingroup.aihdoctorfordoctor.ui.profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.constaint.CollectionConst;
import com.mingroup.aihdoctorfordoctor.data.model.Doctor;
import com.mingroup.aihdoctorfordoctor.data.model.Specialty;
import com.mingroup.aihdoctorfordoctor.ui.MainScreenActivity;
import com.mingroup.aihdoctorfordoctor.ui.welcome.LoginActivity;
import com.mingroup.aihdoctorfordoctor.util.AihLoadingDialog;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;
import com.mingroup.aihdoctorfordoctor.util.StringUtil;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = ProfileFragment.class.getSimpleName();
    private ImageView ivMenu, ivDrAvatar;
    private TextView tvDrName, tvDrTitle, tvDrExperience, tvDrDegree, tvDrSpecialities;
    private Button btnShowMore;
    private ConstraintLayout btnGoToChat, btnGoToCommunity;
    private LinearLayout btnGoToChat_, btnGoToCommunity_;
    private RecyclerView rvChatList;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private AihLoadingDialog aihLoadingDialog;
    private int i = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        aihLoadingDialog = new AihLoadingDialog(getActivity());
        aihLoadingDialog.show();
        init(view);
        displayCurrentDoctorProfile();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        aihLoadingDialog.dismiss();
    }

    private void displayCurrentDoctorProfile() {
        String doctorUid = mAuth.getCurrentUser().getUid();
        db.collection(CollectionConst.COLLECTION_DOCTOR)
                .document(doctorUid)
                .get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (documentSnapshot.exists()) {
                            Doctor doctor = documentSnapshot.toObject(Doctor.class);
                            doctor.setUid(doctorUid);
                            updateProfileUI(doctor);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "get failed with " + e.getMessage());
                    }
                });
    }

    private void updateProfileUI(Doctor doctor) {
        doctor.getSpecialities()
                .forEach(docRef -> {
                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                Specialty specialty = documentSnapshot.toObject(Specialty.class);
                                String specStr = "";
                                if (i == (doctor.getSpecialities().size() - 1)) {
                                    specStr = tvDrSpecialities.getText() + Optional.ofNullable("Khoa " + specialty.getName()).orElse("");
                                } else {
                                    specStr = tvDrSpecialities.getText() + Optional.ofNullable("Khoa " + specialty.getName() + ", ").orElse("");
                                }
                                tvDrSpecialities.setText(specStr);
                                i++;
                            }
                        }
                    });
                });
        ComponentUtil.displayImage(ivDrAvatar, doctor.getPhotoUrl());
        tvDrName.setText(doctor.getName());
        tvDrDegree.setText(doctor.getDegree());
        tvDrExperience.setText(doctor.getExperience() + " năm kinh nghiệm");
        tvDrTitle.setText(doctor.getTitle());
        aihLoadingDialog.dismiss();
    }

    private void init(View view) {
        ivMenu = view.findViewById(R.id.btnMenu);
        ivDrAvatar = view.findViewById(R.id.ivDoctorAvatar);
        tvDrName = view.findViewById(R.id.tvDoctorName);
        tvDrTitle = view.findViewById(R.id.tvDrTitle);
        tvDrExperience = view.findViewById(R.id.tvDrExperience);
        tvDrDegree = view.findViewById(R.id.tvDrDegree);
        tvDrSpecialities = view.findViewById(R.id.tvDrSpecialities);
        btnShowMore = view.findViewById(R.id.btn_chat_list_show_more);
        btnGoToChat = view.findViewById(R.id.btnGoToChat);
        btnGoToCommunity = view.findViewById(R.id.btnGoToCommunity);
        btnGoToChat_ = view.findViewById(R.id.btn_chat_profile);
        btnGoToCommunity_ = view.findViewById(R.id.btn_community_profile);
        rvChatList = view.findViewById(R.id.rv_conservation_list);

        btnGoToCommunity_.setOnClickListener(this);
        btnGoToCommunity.setOnClickListener(this);
        btnGoToChat_.setOnClickListener(this);
        btnGoToChat.setOnClickListener(this);
        ivMenu.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnGoToChat:
            case R.id.btn_chat_profile:
                ((MainScreenActivity) getActivity()).navView.setSelectedItemId(R.id.navigation_conservation);
                break;
            case R.id.btnGoToCommunity:
            case R.id.btn_community_profile:
                ((MainScreenActivity) getActivity()).navView.setSelectedItemId(R.id.navigation_community);
                break;
            case R.id.btnMenu:
                PopupMenu popupMenu = new PopupMenu(getActivity(), ivMenu);

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.menu_logout:
                                logoutCurrentUser();
                                break;
                            case R.id.menu_about:
                                Toast.makeText(getActivity(), "Đã click", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.getMenuInflater().inflate(R.menu.profile_menu, popupMenu.getMenu());
                popupMenu.show();
                break;
        }
    }

    private void logoutCurrentUser() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Thông báo")
                .setMessage("Bạn có chắn chắn muốn thoát?")
                .setPositiveButton("Đăng xuất", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mAuth.signOut();
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create()
                .show();
    }
}
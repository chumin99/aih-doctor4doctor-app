package com.mingroup.aihdoctorfordoctor.ui.community.unansweredquestion;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.constaint.CollectionConst;
import com.mingroup.aihdoctorfordoctor.data.model.Doctor;
import com.mingroup.aihdoctorfordoctor.data.model.Question;
import com.mingroup.aihdoctorfordoctor.data.model.Specialty;
import com.mingroup.aihdoctorfordoctor.ui.community.QuestionAdapter;
import com.mingroup.aihdoctorfordoctor.ui.community.SpecialitiesAdapter;
import com.mingroup.aihdoctorfordoctor.ui.community.questiondetail.QuestionDetailActivity;
import com.mingroup.aihdoctorfordoctor.util.AihLoadingDialog;
import com.mingroup.aihdoctorfordoctor.util.FileUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class UnAnsweredQuestionFragment extends Fragment implements QuestionAdapter.OnQuestionListener, SpecialitiesAdapter.OnSpecialtyListener {
    private static final String TAG = UnAnsweredQuestionFragment.class.getSimpleName();
    private RecyclerView rvSpecialities, rvQuestions;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    // Specialities Section
    private List<Specialty> specialties;
    private SpecialitiesAdapter specAdapter;

    // Question Section
    private List<Question> questions;
    private QuestionAdapter questionAdapter;

    private AihLoadingDialog aihLoadingDialog;
    private List<DocumentReference> specialtiesList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        aihLoadingDialog = new AihLoadingDialog(getActivity());
        aihLoadingDialog.show();
        return inflater.inflate(R.layout.fragment_un_answered_question, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    @Override
    public void onStart() {
        super.onStart();
        setupQuestionSection();
        setupSpecialitiesSection();
    }

    private void setupQuestionSection() {
        questions = new ArrayList<>();
        questionAdapter = new QuestionAdapter(questions, this);
        rvQuestions.setAdapter(questionAdapter);
    }

    private void populateQuestionsToRv(List<DocumentReference> specialitiesDocRef) {
        db.collection(CollectionConst.COLLECTION_QUESTION)
                .whereEqualTo("reply", null)
                .whereIn("specialty", specialitiesDocRef)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questions.add(question);
                            }
                            questionAdapter.submitList(questions);
                        } else {
                            Toast.makeText(getContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        aihLoadingDialog.dismiss();
    }

    private void setupSpecialitiesSection() {
        specialties = new ArrayList<>();
        Uri allImageUri = FileUtil.convertFromResource(getResources(), R.drawable.all_specialty_colored);
        specialties.add(0, new Specialty("default", "Tất cả", allImageUri.toString()));
        specAdapter = new SpecialitiesAdapter(getActivity(), specialties, true, this);
        rvSpecialities.setAdapter(specAdapter);
        populateSpecialitiesToRv();
    }

    private void populateSpecialitiesToRv() {
        mAuth = FirebaseAuth.getInstance();
        db.collection(CollectionConst.COLLECTION_DOCTOR)
                .document(mAuth.getCurrentUser().getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot documentSnapshot = task.getResult();
                            if (documentSnapshot != null) {
                                Doctor doctor = documentSnapshot.toObject(Doctor.class);
                                specialtiesList = doctor.getSpecialities();
                                populateQuestionsToRv(specialtiesList);
                                for (DocumentReference docRef : specialtiesList) {
                                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                                        @Override
                                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                                            Specialty specialty = documentSnapshot.toObject(Specialty.class);
                                            specialty.setId(docRef.getId());
                                            specialties.add(specialty);
                                            specAdapter.submitList(specialties);
                                        }
                                    });
                                }
                            }
                        } else {
                            Toast.makeText(getContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void init(View view) {
        rvSpecialities = view.findViewById(R.id.rvSpecialities);
        rvQuestions = view.findViewById(R.id.rvQuestion);
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onQuestionClickListener(int position) {
        Question item = questionAdapter.getItem(position);
        Intent intent = new Intent(getActivity(), QuestionDetailActivity.class);
        intent.putExtra("question", Parcels.wrap(item));
        startActivity(intent);
    }

    @Override
    public void onSpecialtyClickListener(int position) {

    }

    @Override
    public void onSpecialtyCheckedChangeListener(View view, boolean isChecked, int position) {
        if(!specialtiesList.isEmpty()) {
            if(isChecked) {
                if(position == 0) {
                    aihLoadingDialog.show();
                    questionAdapter.getFilter().filter("");
                    aihLoadingDialog.dismiss();
                } else {
                    aihLoadingDialog.show();
                    questionAdapter.getFilter().filter(specAdapter.getItem(position).getId());
                    aihLoadingDialog.dismiss();
                }
            }
        }
    }
}
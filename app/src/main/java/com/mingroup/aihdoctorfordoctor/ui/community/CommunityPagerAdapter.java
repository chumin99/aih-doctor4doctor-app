package com.mingroup.aihdoctorfordoctor.ui.community;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.mingroup.aihdoctorfordoctor.ui.community.myansweredquestion.MyAnsweredQuestion;
import com.mingroup.aihdoctorfordoctor.ui.community.unansweredquestion.UnAnsweredQuestionFragment;

public class CommunityPagerAdapter extends FragmentStateAdapter {

    public CommunityPagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new UnAnsweredQuestionFragment();
            case 1:
                return new MyAnsweredQuestion();
            default:
                return new UnAnsweredQuestionFragment();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}

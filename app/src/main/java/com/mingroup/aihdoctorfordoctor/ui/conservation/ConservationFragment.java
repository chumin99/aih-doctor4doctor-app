package com.mingroup.aihdoctorfordoctor.ui.conservation;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.constaint.CollectionConst;
import com.mingroup.aihdoctorfordoctor.data.model.Conservation;
import com.mingroup.aihdoctorfordoctor.ui.MainScreenActivity;
import com.mingroup.aihdoctorfordoctor.ui.conservation.chatwindow.ChatActivity;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class ConservationFragment extends Fragment implements ConservationListAdapter.OnConservationListener {
    private static final String TAG = ConservationFragment.class.getSimpleName();
    private Toolbar mToolbar;
    private RecyclerView rvConservations;
    private ConservationListAdapter conservationListAdapter;
    private List<Conservation> conservationList;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conservation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view) {
        mToolbar = view.findViewById(R.id.aihToolbar);
        rvConservations = view.findViewById(R.id.rv_conservation_list);
        conservationList = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        setupToolbar();
        setupConservationList();
    }

    private void setupConservationList() {
        conservationListAdapter = new ConservationListAdapter(getActivity(), conservationList, this);
        rvConservations.setAdapter(conservationListAdapter);
        populateDataToConservationRv();
    }

    private void setupToolbar() {
        ((MainScreenActivity) getActivity()).setSupportActionBar(mToolbar);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setTitle(null);

        TextView tvTitle = mToolbar.findViewById(R.id.toolbar_title);
        tvTitle.setText("Danh sách tư vấn");
        tvTitle.setTextColor(getResources().getColor(android.R.color.black, getActivity().getTheme()));
        ((Toolbar.LayoutParams) tvTitle.getLayoutParams()).gravity = Gravity.CENTER_HORIZONTAL;
        setHasOptionsMenu(true);
    }

    private void populateDataToConservationRv() {
        mAuth = FirebaseAuth.getInstance();
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .whereEqualTo("doctor", db.collection(CollectionConst.COLLECTION_DOCTOR).document(mAuth.getCurrentUser().getUid()))
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if (error != null) {
                            Log.w(TAG, "Listen failed.", error);
                            Toast.makeText(getActivity(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if(value != null) {
                            conservationList = value.toObjects(Conservation.class);
                            Log.d(TAG, conservationList.toString());
                            conservationListAdapter.submitList(conservationList);
                        }
                    }
                });
    }

    @Override
    public void onConservationClickListener(int position) {
        Conservation conservation = conservationListAdapter.getItem(position);
        Intent intent = new Intent(getContext(), ChatActivity.class);
        intent.putExtra("conservation", Parcels.wrap(conservation));
        startActivity(intent);
    }

    @Override
    public boolean onConservationLongClickListener(View view, int position) {
        return false;
    }
}
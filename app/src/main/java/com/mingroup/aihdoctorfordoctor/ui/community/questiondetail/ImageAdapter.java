package com.mingroup.aihdoctorfordoctor.ui.community.questiondetail;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private List<String> listImageUrl;
    private OnImageListener onImageListener;

    public ImageAdapter(List<String> listImageUrl, OnImageListener onImageListener) {
        this.listImageUrl = listImageUrl;
        this.onImageListener = onImageListener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_detail_question_image, parent, false), onImageListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        String photoUrl = listImageUrl.get(position);
        ComponentUtil.displayImage(holder.getImageView(), photoUrl);
    }

    @Override
    public int getItemCount() {
        return listImageUrl.size();
    }

    @Getter
    public class ImageViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView imageView;
        private OnImageListener onImageListener;

        public ImageViewHolder(@NonNull View itemView, OnImageListener onImageListener) {
            super(itemView);
            this.onImageListener = onImageListener;
            imageView = itemView.findViewById(R.id.iv_item);
            itemView.setOnClickListener(view -> {
                if (onImageListener != null) {
                    int position = getAdapterPosition();
                    if(position!=RecyclerView.NO_POSITION)
                        onImageListener.onImageClickListener(position);
                }
            });
        }
    }

    public interface OnImageListener {
        void onImageClickListener(int position);
    }
}

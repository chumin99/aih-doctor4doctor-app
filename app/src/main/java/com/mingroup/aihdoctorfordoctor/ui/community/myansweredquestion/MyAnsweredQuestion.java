package com.mingroup.aihdoctorfordoctor.ui.community.myansweredquestion;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.constaint.CollectionConst;
import com.mingroup.aihdoctorfordoctor.data.model.Question;
import com.mingroup.aihdoctorfordoctor.ui.community.QuestionAdapter;
import com.mingroup.aihdoctorfordoctor.util.AihLoadingDialog;

import java.util.ArrayList;
import java.util.List;

public class MyAnsweredQuestion extends Fragment implements QuestionAdapter.OnQuestionListener {
    // Question Section
    private List<Question> questions;
    private QuestionAdapter questionAdapter;
    private RecyclerView rvQuestions;

    private AihLoadingDialog aihLoadingDialog;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        aihLoadingDialog = new AihLoadingDialog(getActivity());
        aihLoadingDialog.show();
        return inflater.inflate(R.layout.fragment_my_answered_question, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvQuestions = view.findViewById(R.id.rvQuestion);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        setupQuestionSection();
    }

    private void setupQuestionSection() {
        questions = new ArrayList<>();
        questionAdapter = new QuestionAdapter(questions, this);
        rvQuestions.setAdapter(questionAdapter);
        populateQuestionsToRv();
    }

    private void populateQuestionsToRv() {
        String currentDoctorId = mAuth.getCurrentUser().getUid();
        DocumentReference doctorRef = db.document(CollectionConst.COLLECTION_DOCTOR + "/" + currentDoctorId);

        db.collection(CollectionConst.COLLECTION_QUESTION)
                .whereNotEqualTo("reply", null)
                .whereEqualTo("reply.fromDoctor", doctorRef)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questions.add(question);
                            }
                            questionAdapter.submitList(questions);
                        } else {
                            Log.d("Exception", task.getException().getMessage());
                            Toast.makeText(getContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        aihLoadingDialog.dismiss();
    }
    @Override
    public void onQuestionClickListener(int position) {

    }
}
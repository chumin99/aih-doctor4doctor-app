package com.mingroup.aihdoctorfordoctor.ui.conservation.chatwindow;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.constaint.CollectionConst;
import com.mingroup.aihdoctorfordoctor.constaint.StorageConst;
import com.mingroup.aihdoctorfordoctor.data.model.Conservation;
import com.mingroup.aihdoctorfordoctor.data.model.Doctor;
import com.mingroup.aihdoctorfordoctor.data.model.Message;
import com.mingroup.aihdoctorfordoctor.data.model.MessageType;
import com.mingroup.aihdoctorfordoctor.data.model.User;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;
import com.mingroup.aihdoctorfordoctor.util.StringUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;
import gun0912.tedbottompicker.TedBottomSheetDialogFragment;

public class ChatActivity extends AppCompatActivity implements ChatListAdapter.OnChatItemListener {
    private static final String TAG = ChatActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private TextView tvReceiverName, rvReceiverStatus, tvAlert;
    private ImageView ivReceiverAva;
    private RecyclerView rvChatList;
    private ImageButton ibnAttachment, btnSend;
    private TextInputLayout tipInputText;
    private Conservation conservation;
    private List<Message> messageList;
    private Doctor doctor;
    private User patient;
    private FirebaseAuth mAuth;
    private String patientUid, doctorUid;
    private ChatListAdapter chatListAdapter;
    private FirebaseFirestore db;
    private ListenerRegistration chatRegistration;
    private boolean isConservationCreated = true;
    private StorageReference mStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        init();
        setupToolbar();
        setupChatListRv();

        ibnAttachment.setOnClickListener(view -> {
            TedPermission.with(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            pickImageFromGallery();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Toast.makeText(ChatActivity.this, "Cấp phép quyền truy cập để tiếp tục", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .check();
        });

        btnSend.setOnClickListener(view -> {
            doSendMessage();
        });
    }

    private void setupChatListRv() {
        messageList = new ArrayList<>();
        chatListAdapter = new ChatListAdapter(this, messageList, this);
        rvChatList.setAdapter(chatListAdapter);

        if(conservation != null) {
            listenForChatMessages();
        } else {
            db.collection(CollectionConst.COLLECTION_CONSERVATION)
                    .whereEqualTo("doctor", db.collection(CollectionConst.COLLECTION_DOCTOR).document(doctor.getUid()))
                    .whereEqualTo("patient", db.collection(CollectionConst.COLLECTION_USER).document(patientUid))
                    .get()
                    .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            List<Conservation> conservationList = queryDocumentSnapshots.toObjects(Conservation.class);
                            if(!conservationList.isEmpty()) {
                                conservation = conservationList.get(0);
                                Log.d(TAG, conservation.toString());
                                listenForChatMessages();
                            }
                        }
                    });
        }

    }

    private void listenForChatMessages() {
        chatRegistration = db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .document(conservation.getId())
                .collection("messageList")
                .orderBy("timestamp", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if (error != null) {
                            Log.w(TAG, "Listen failed.", error);
                            return;
                        }

                        if(value != null) {
                            chatListAdapter.submitList(value.toObjects(Message.class));
                            if(chatListAdapter.getItemCount() > 0)
                                rvChatList.smoothScrollToPosition(chatListAdapter.getItemCount() - 1);
                        }
                    }
                });
    }

    private void doSendMessage() {
        String messageText = ComponentUtil.getInputText(tipInputText);
        if(!messageText.isEmpty()) {
                createNewMessage(messageText, MessageType.TEXT);
        }
    }

    private void createNewMessage(String messageText, MessageType messageType) {
        Message message = new Message(messageText, doctorUid, patientUid, new Date(), messageType);
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .document(conservation.getId())
                .collection("messageList")
                .add(message)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        if(chatListAdapter.getItemCount() > 0)
                            rvChatList.smoothScrollToPosition(chatListAdapter.getItemCount() - 1);
                        tipInputText.getEditText().setText("");

                        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                                .document(conservation.getId())
                                .update("lastMessage", message)
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "update last message successfully!");
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return false;
    }

    private void pickImageFromGallery() {
        TedBottomPicker.with(this)
                .setTitle("Chọn ảnh")
                .setCompleteButtonText("Xong")
                .show(new TedBottomSheetDialogFragment.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        uploadImage(uri);
                    }
                });
    }

    private void uploadImage(Uri imageUri) {
        StorageReference avatarRef = mStorageRef.child(StorageConst.CHAT_FOLDER).child(StringUtil.generateRandomFileName());
        avatarRef.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                createNewMessage(uri.toString(), MessageType.IMAGE);
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "upload hình thất bại::" + e.getMessage());
                    }
                });
    }


    private void displayCurrentReceiverProfile() {
        tvReceiverName.setText(patient.getDisplayName());
        ComponentUtil.displayImage(ivReceiverAva, patient.getPhotoUrl());
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbar);
        tvReceiverName = findViewById(R.id.tv_receiver_name);
        rvReceiverStatus = findViewById(R.id.tv_receiver_status);
        ivReceiverAva = findViewById(R.id.iv_receiver);
        rvChatList = findViewById(R.id.rvChatList);
        ibnAttachment = findViewById(R.id.ibnAttachment);
        btnSend = findViewById(R.id.btnSendChat);
        tipInputText = findViewById(R.id.tipInput);
        tvAlert = findViewById(R.id.tvAlert);
        mAuth = FirebaseAuth.getInstance();
        messageList = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        patient = new User();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        if(getIntent().getExtras() != null) {
            conservation = Parcels.unwrap(getIntent().getParcelableExtra("conservation"));
            if(conservation != null) {
                conservation.getPatient().get().addOnSuccessListener(documentSnapshot -> {
                    if(documentSnapshot.exists()) {
                        patient = documentSnapshot.toObject(User.class);
                        patient.setUid(documentSnapshot.getId());
                        patientUid = documentSnapshot.getId();
                        displayCurrentReceiverProfile();
                    }
                });
            }
            doctorUid = mAuth.getCurrentUser().getUid();
        }

    }

    @Override
    public void onChatItemLongClickListener(int position) {

    }

    @Override
    public void onChatItemClickListener(int position) {

    }
}
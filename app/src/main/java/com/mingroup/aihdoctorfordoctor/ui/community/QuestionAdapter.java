package com.mingroup.aihdoctorfordoctor.ui.community;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.data.model.Doctor;
import com.mingroup.aihdoctorfordoctor.data.model.Question;
import com.mingroup.aihdoctorfordoctor.data.model.Reply;
import com.mingroup.aihdoctorfordoctor.data.model.Specialty;
import com.mingroup.aihdoctorfordoctor.data.model.User;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.mingroup.aihdoctorfordoctor.util.StringUtil.calculateAge;
import static com.mingroup.aihdoctorfordoctor.util.StringUtil.generateLastTime;
import static com.mingroup.aihdoctorfordoctor.util.StringUtil.isMaleOrFemale;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> implements Filterable {

    @AllArgsConstructor
    @Getter
    enum QuestionStatus {
        UNANSWERED(0),
        ANSWERED(1);
        private int typeId;

        public static QuestionStatus getQuestionStatus(final int id) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Stream.of(QuestionStatus.values())
                        .filter(targetStatus -> targetStatus.typeId == id).findFirst().get();
            } else {
                for (QuestionStatus item : QuestionStatus.values()) {
                    if (item.typeId == id)
                        return item;
                }
            }
            return null;
        }
    }
    private List<Question> questions;
    private List<Question> filteredQuestions;
    private OnQuestionListener onQuestionListener;
    private AsyncListDiffer<Question> mDiffer;
    private DiffUtil.ItemCallback<Question> diffCallback = new DiffUtil.ItemCallback<Question>() {
        @Override
        public boolean areItemsTheSame(@NonNull Question oldItem, @NonNull Question newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Question oldItem, @NonNull Question newItem) {
            return oldItem.getContent().equals(newItem.getContent());
        }
    };

    public QuestionAdapter(List<Question> questions, OnQuestionListener onQuestionListener) {
        this.questions = questions;
        this.filteredQuestions = questions;
        this.onQuestionListener = onQuestionListener;
        this.mDiffer = new AsyncListDiffer<Question>(this, diffCallback);
    }

    @Override
    public int getItemViewType(int position) {
        Question question = getItem(position);
        if(question.getReply() == null) {
            return QuestionStatus.UNANSWERED.typeId;
        }
        return QuestionStatus.ANSWERED.typeId;
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        QuestionStatus status = QuestionStatus.getQuestionStatus(viewType);
        if(status == QuestionStatus.UNANSWERED)
            return new QuestionViewHolder(layoutInflater.inflate(R.layout.item_unaswered_question, parent, false), status, onQuestionListener);
        return new QuestionViewHolder(layoutInflater.inflate(R.layout.item_answered_question, parent, false), status, onQuestionListener);
    }

    @Override
    public void onBindViewHolder(@NonNull QuestionViewHolder holder, int position) {
        Question question = getItem(position);

        question.getSpecialty().get().addOnSuccessListener(documentSnapshot -> {
            Specialty specialty = documentSnapshot.toObject(Specialty.class);
            ComponentUtil.displayImage(holder.getSpecIcon(), specialty.getIconUrl());
            holder.getSpecName().setText(specialty.getName());
        });

        question.getFromUser().get().addOnSuccessListener(documentSnapshot -> {
            User user = documentSnapshot.toObject(User.class);
            String userInfo = isMaleOrFemale(user.isMale()) + " " + calculateAge(user.getDob());
            holder.getTvUserInfo().setText(userInfo);
        });

        // question section
        holder.getTvQuestionContent().setText(question.getContent());
        holder.getTvTime().setText(generateLastTime(question.getCreatedAt()));

        if(question.getReply() != null) {
            Reply reply = question.getReply();
            holder.getTvDrAnswer().setText(reply.getContent());
            reply.getFromDoctor().get().addOnSuccessListener(documentSnapshot -> {
                Doctor doctor = documentSnapshot.toObject(Doctor.class);
                holder.getTvDrName().setText(doctor.getName());
                ComponentUtil.displayImage(holder.getIvDrAvatar(), doctor.getPhotoUrl());
            });
        }
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public Question getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    public void submitList(List<Question> newList) {
        mDiffer.submitList(newList);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if(charSequence == null || charSequence.length() == 0) {
                    filterResults.count = filteredQuestions.size();
                    filterResults.values = filteredQuestions;
                } else {
                    String strQuery = charSequence.toString().toLowerCase();
                    List<Question> resultList = new ArrayList<>();
                    if(resultList.isEmpty()){
                        for(Question question : filteredQuestions) {
                            if(question.getSpecialty().getId().equalsIgnoreCase(strQuery)) {
                                resultList.add(question);
                            }
                        }
                    }
                    filterResults.count = resultList.size();
                    filterResults.values = resultList;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                questions = (List<Question>) filterResults.values;
                submitList(questions);
            }
        };
    }

    @Getter
    public class QuestionViewHolder extends RecyclerView.ViewHolder {
        private RoundedImageView specIcon, ivDrAvatar;
        private TextView specName, tvUserInfo, tvTime, tvQuestionContent, tvDrName, tvDrAnswer;
        private Button btnDetail;
        private OnQuestionListener onReplyListener;

        public QuestionViewHolder(@NonNull View itemView, QuestionStatus status, OnQuestionListener onReplyListener) {
            super(itemView);
            this.onReplyListener = onReplyListener;

            specIcon = itemView.findViewById(R.id.specialty_icon);
            specName = itemView.findViewById(R.id.specialty_name);
            tvQuestionContent = itemView.findViewById(R.id.tvQuestionContent);
            tvUserInfo = itemView.findViewById(R.id.tvUserInfo);
            tvTime = itemView.findViewById(R.id.tvTime);
            btnDetail = itemView.findViewById(R.id.btnDetail);

            if(status == QuestionStatus.ANSWERED) {
                tvDrName = itemView.findViewById(R.id.tvDoctorName);
                tvDrAnswer = itemView.findViewById(R.id.tvReplyContent);
                ivDrAvatar = itemView.findViewById(R.id.ivDoctorAvatar);
            }

            itemView.setOnClickListener(view -> {
                if(onReplyListener != null) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION)
                        onReplyListener.onQuestionClickListener(position);
                }
            });

            btnDetail.setOnClickListener(view -> {
                itemView.performClick();
            });

        }
    }

    public interface OnQuestionListener {
        void onQuestionClickListener(int position);
    }
}

package com.mingroup.aihdoctorfordoctor.ui.community.questiondetail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.constaint.CollectionConst;
import com.mingroup.aihdoctorfordoctor.data.model.Doctor;
import com.mingroup.aihdoctorfordoctor.data.model.Question;
import com.mingroup.aihdoctorfordoctor.data.model.Reply;
import com.mingroup.aihdoctorfordoctor.data.model.Specialty;
import com.mingroup.aihdoctorfordoctor.data.model.User;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.mingroup.aihdoctorfordoctor.util.StringUtil.calculateAge;
import static com.mingroup.aihdoctorfordoctor.util.StringUtil.generateLastTime;
import static com.mingroup.aihdoctorfordoctor.util.StringUtil.isMaleOrFemale;

public class QuestionDetailActivity extends AppCompatActivity implements ImageAdapter.OnImageListener {
    private Toolbar mToolbar;

    // question section
    private ImageView ivUserAvatar;
    private TextView tvUserInfo, tvQuestionTimeStamps, tvQuestionContent;
    private RecyclerView rvAttachmentImg;

    // specialty section
    private ImageView ivSpecIcon;
    private TextView tvSpecName;

    // reply section
    private LinearLayout emptyAnswerSection;
    private RelativeLayout drReplySection;
    private ImageView ivDoctorAvatar;
    private TextView tvDrName, tvReplyContent;

    // answer input section
    private TextInputLayout tipReply;
    private ImageButton ibnSend;

    private FirebaseFirestore db;
    private Question question;
    private FirebaseAuth mAuth;

    private List<String> listPhotoUrl = new ArrayList<>();
    private ImageAdapter imageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);
        init();
        setupToolbar();
        displayCurrentQuestion();
        tipReply.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    ibnSend.setClickable(false);
                    ibnSend.setAlpha(.5f);
                } else {
                    ibnSend.setClickable(true);
                    ibnSend.setAlpha(1f);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty()) {
                    ibnSend.setClickable(false);
                    ibnSend.setAlpha(.5f);
                } else {
                    ibnSend.setClickable(true);
                    ibnSend.setAlpha(1f);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().isEmpty()) {
                    ibnSend.setClickable(false);
                    ibnSend.setAlpha(.5f);
                } else {
                    ibnSend.setClickable(true);
                    ibnSend.setAlpha(1f);
                }
            }
        });
        ibnSend.setOnClickListener(view -> {
            doSendReply();
        });
    }

    private void doSendReply() {
        String replyContent = ComponentUtil.getInputText(tipReply);
        Reply reply = new Reply();
        reply.setContent(replyContent);
        reply.setCreatedAt(new Date());
        reply.setFromDoctor(db.document(CollectionConst.COLLECTION_DOCTOR + "/" + mAuth.getCurrentUser().getUid()));
        reply.setLove_count(0);

        db.collection(CollectionConst.COLLECTION_QUESTION)
                .document(question.getId())
                .update(
                        "reply", reply
                ).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    reply.getFromDoctor().get().addOnSuccessListener(documentSnapshot -> {
                        if (documentSnapshot.exists()) {
                            Doctor doctor = documentSnapshot.toObject(Doctor.class);
                            ComponentUtil.displayImage(ivDoctorAvatar, doctor.getPhotoUrl());
                            tvDrName.setText(doctor.getName());
                        }
                    });
                    tvReplyContent.setText(replyContent);
                    emptyAnswerSection.setVisibility(View.GONE);
                    drReplySection.setVisibility(View.VISIBLE);

                    tipReply.getEditText().setText("");
                    tipReply.setEnabled(false);
                    ibnSend.setEnabled(false);
                } else {
                    Toast.makeText(QuestionDetailActivity.this, "Gửi câu trả lời xày ra lỗi, Thử lại", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void displayCurrentQuestion() {
        listPhotoUrl.addAll(question.getImages());
        imageAdapter = new ImageAdapter(listPhotoUrl, this);
        rvAttachmentImg.setAdapter(imageAdapter);

        question.getFromUser().get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                User fromUser = documentSnapshot.toObject(User.class);
                if (fromUser != null) {
                    ComponentUtil.displayImage(ivUserAvatar, fromUser.getPhotoUrl());
                    String userInfo = isMaleOrFemale(fromUser.isMale()) + " " + calculateAge(fromUser.getDob()) + " tuổi";
                    tvUserInfo.setText(userInfo);
                }
            }
        });
        tvQuestionTimeStamps.setText(generateLastTime(question.getCreatedAt()));
        tvQuestionContent.setText(question.getContent());

        question.getSpecialty().get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                Specialty specialty = documentSnapshot.toObject(Specialty.class);
                if (specialty != null) {
                    ComponentUtil.displayImage(ivSpecIcon, specialty.getIconUrl());
                    tvSpecName.setText(specialty.getName());
                }
            }
        });


    }

    private void setupToolbar() {
        mToolbar.setTitle("Chi tiết câu hỏi");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbar);
        ivUserAvatar = findViewById(R.id.ques_detail_user_avatar);
        tvUserInfo = findViewById(R.id.ques_detail_user_info);
        tvQuestionTimeStamps = findViewById(R.id.ques_detail_timestamp);
        tvQuestionContent = findViewById(R.id.ques_detail_content);
        rvAttachmentImg = findViewById(R.id.rv_attachment_img);
        ivSpecIcon = findViewById(R.id.specialty_icon);
        tvSpecName = findViewById(R.id.specialty_name);
        ivDoctorAvatar = findViewById(R.id.ivDoctorAvatar);
        tvDrName = findViewById(R.id.tvDoctorName);
        tvReplyContent = findViewById(R.id.tvReplyContent);
        tipReply = findViewById(R.id.tip_answer);
        ibnSend = findViewById(R.id.ibnSend);
        emptyAnswerSection = findViewById(R.id.empty_answer);
        drReplySection = findViewById(R.id.dr_reply_section);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        if (getIntent().getExtras() != null) {
            question = (Question) Parcels.unwrap(getIntent().getParcelableExtra("question"));
        }
    }

    @Override
    public void onImageClickListener(int position) {

    }
}
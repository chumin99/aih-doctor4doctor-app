package com.mingroup.aihdoctorfordoctor.ui.conservation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.data.model.Conservation;
import com.mingroup.aihdoctorfordoctor.data.model.Doctor;
import com.mingroup.aihdoctorfordoctor.data.model.Message;
import com.mingroup.aihdoctorfordoctor.data.model.MessageType;
import com.mingroup.aihdoctorfordoctor.data.model.User;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;

import org.ocpsoft.prettytime.PrettyTime;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class ConservationListAdapter extends RecyclerView.Adapter<ConservationListAdapter.ConservationVH> {
    private Context context;
    private List<Conservation> conservationList;
    private OnConservationListener onConservationListener;
    private AsyncListDiffer<Conservation> mDiffer;
    private PrettyTime prettyTime;
    private DiffUtil.ItemCallback<Conservation> diffCallback = new DiffUtil.ItemCallback<Conservation>() {
        @Override
        public boolean areItemsTheSame(@NonNull Conservation oldItem, @NonNull Conservation newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Conservation oldItem, @NonNull Conservation newItem) {
            return oldItem.getDoctor().equals(newItem.getDoctor()) &&
                    oldItem.getPatient().equals(newItem.getPatient());
        }
    };

    public ConservationListAdapter(Context context, List<Conservation> conservationList, OnConservationListener onConservationListener) {
        this.context = context;
        this.conservationList = conservationList;
        this.onConservationListener = onConservationListener;
        this.mDiffer = new AsyncListDiffer<Conservation>(this, diffCallback);
        this.prettyTime = new PrettyTime(ComponentUtil.getCurrentLocale(context));
    }

    @NonNull
    @Override
    public ConservationVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ConservationVH(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat_conservation, parent, false), onConservationListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ConservationVH holder, int position) {
        Conservation conservation = getItem(position);

        conservation.getPatient().get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                User patient = documentSnapshot.toObject(User.class);
                if (patient != null) {
                    ComponentUtil.displayImage(holder.getIvAvatar(), patient.getPhotoUrl());
                    holder.getTvConservationName().setText(patient.getDisplayName());
                }
            }
        });

        Message lastMessage = conservation.getLastMessage();
        if(lastMessage != null){
            if(lastMessage.getType() == MessageType.TEXT) {
                holder.getTvLastMessage().setText(lastMessage.getContent());
            } else {
                holder.getTvLastMessage().setText("[Hình Ảnh]");
            }
            holder.getTvLastTime().setText(prettyTime.format(lastMessage.getTimestamp()));
        }

    }

    public Conservation getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public void submitList(List<Conservation> newList){
        mDiffer.submitList(newList != null ? new ArrayList<>(newList) : null);
    }

    @Getter
    public class ConservationVH extends RecyclerView.ViewHolder {
        private ImageView ivAvatar;
        private TextView tvConservationName, tvLastMessage, tvLastTime, tvNotificationCount;
        private OnConservationListener onConservationListener;

        public ConservationVH(@NonNull View itemView, OnConservationListener onConservationListener) {
            super(itemView);
            this.onConservationListener = onConservationListener;
            ivAvatar = itemView.findViewById(R.id.chat_conservation_photo);
            tvConservationName = itemView.findViewById(R.id.chat_conservation_name);
            tvLastMessage = itemView.findViewById(R.id.chat_conservation_last_message);
            tvLastTime = itemView.findViewById(R.id.chat_conservation_last_time);
            tvNotificationCount = itemView.findViewById(R.id.chat_conservation_notification_count);
            itemView.setOnClickListener(view -> {
                if (onConservationListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                        onConservationListener.onConservationClickListener(position);
                }
            });

            itemView.setOnLongClickListener(view -> {
                if (onConservationListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onConservationListener.onConservationClickListener(position);
                        return true;
                    }
                }
                return false;
            });
        }
    }

    public interface OnConservationListener {
        void onConservationClickListener(int position);

        boolean onConservationLongClickListener(View view, int position);
    }
}

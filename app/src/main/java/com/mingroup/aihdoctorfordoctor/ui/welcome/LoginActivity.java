package com.mingroup.aihdoctorfordoctor.ui.welcome;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.mingroup.aihdoctorfordoctor.R;
import com.mingroup.aihdoctorfordoctor.ui.MainScreenActivity;
import com.mingroup.aihdoctorfordoctor.util.ComponentUtil;
import com.mingroup.aihdoctorfordoctor.util.ValidatorUtil;

public class LoginActivity extends AppCompatActivity {
    private LottieAnimationView lavIntro;
    private TextInputLayout tipEmail, tipPassword;
    private Button btnLogin;
    private FirebaseAuth mAuth;
    private TextView tvLoginError;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        btnLogin.setOnClickListener(view -> {
            tvLoginError.setVisibility(View.GONE);
            String email = ComponentUtil.getInputText(tipEmail).trim() + ValidatorUtil.DR_AIH_SUFFIX_EMAIL;
            String password = ComponentUtil.getInputText(tipPassword);

            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                navigateToMainScreen();
                            } else {
                                tvLoginError.setVisibility(View.VISIBLE);
                            }
                        }
                    });
        });
    }

    private void navigateToMainScreen() {
        Intent intent = new Intent(this, MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void init() {
        lavIntro = findViewById(R.id.lavIntro);
        tipEmail = findViewById(R.id.tipEmail);
        tipPassword = findViewById(R.id.tipPassword);
        btnLogin = findViewById(R.id.btnLogin);
        mAuth = FirebaseAuth.getInstance();
        tvLoginError = findViewById(R.id.tvLoginError);
    }
}
package com.mingroup.aihdoctorfordoctor.ui.community;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.mingroup.aihdoctorfordoctor.R;

public class CommunityFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager2 vpCommunity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_community, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        setupTabPage();
    }

    private void setupTabPage() {
        CommunityPagerAdapter communityPagerAdapter =
                new CommunityPagerAdapter(getActivity());
        vpCommunity.setAdapter(communityPagerAdapter);
        new TabLayoutMediator(tabLayout, vpCommunity, ((tab, position) -> {
            switch (position) {
                case 0: {
                    tab.setText("Câu hỏi cộng đồng");
                    tab.setIcon(R.drawable.ic_pending_question);
                    BadgeDrawable badgeDrawable = tab.getOrCreateBadge();
                    badgeDrawable.setBackgroundColor(getResources().getColor(R.color.dark_blue, getActivity().getTheme()));
                    badgeDrawable.setVisible(true);
                    badgeDrawable.setNumber(123);
                    badgeDrawable.setMaxCharacterCount(3);
                    break;
                }
                case 1: {
                    tab.setText("Trả lời của tôi");
                    tab.setIcon(R.drawable.ic_replied_question);
                    break;
                }
            }
        })).attach();
    }

    private void init(View view) {
        tabLayout = view.findViewById(R.id.community_tab_layout);
        vpCommunity = view.findViewById(R.id.vpCommunity);
    }
}
package com.mingroup.aihdoctorfordoctor.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.DrawableCrossFadeFactory;
import com.google.android.material.textfield.TextInputLayout;
import com.mingroup.aihdoctorfordoctor.R;

import java.util.Locale;

public class ComponentUtil {
    private static RequestListener<Drawable> createLoggerListener(final String name) {
        return new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target target, boolean isFirstResource) {
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target target, DataSource dataSource, boolean isFirstResource) {
                if (resource instanceof BitmapDrawable) {
                    Bitmap bitmap = ((BitmapDrawable) resource).getBitmap();
                }
                return false;
            }
        };
    };

    public static String getInputText(TextInputLayout tip) {
        return tip.getEditText().getText().toString();
    }

    public static void setInputText(TextInputLayout tip, String content) {
        tip.getEditText().setText(content);
    }

    public static void displayImage(ImageView imageView, String photoUrl) {
        Context context = imageView.getContext();
        Glide.with(context)
                .load(photoUrl)
                .dontTransform()
                .listener(createLoggerListener("match_image"))
                .override(Target.SIZE_ORIGINAL)
                .format(DecodeFormat.PREFER_ARGB_8888)
                .placeholder(new ColorDrawable(context.getResources().getColor(R.color.icon_tint, context.getTheme())))
                .error(R.drawable.error_image)
                .into(imageView);
    }

    public static Locale getCurrentLocale(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
            return context.getResources().getConfiguration().getLocales().get(0);
        } else{
            return context.getResources().getConfiguration().locale;
        }
    }
}

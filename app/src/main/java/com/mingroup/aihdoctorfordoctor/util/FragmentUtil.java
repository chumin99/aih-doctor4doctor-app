package com.mingroup.aihdoctorfordoctor.util;

import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentUtil {

    /**
     * @param fragmentActivity
     * @param containerViewId
     * @param destFragment
     * @param bundle
     * @param tag
     * @param addToBackStack
     */
    public static void replaceFragment(FragmentActivity fragmentActivity,
                                       int containerViewId,
                                       Fragment destFragment,
                                       Bundle bundle,
                                       String tag,
                                       boolean addToBackStack) {
        try {
            if (destFragment == null)
                return;

            if (bundle != null)
                destFragment.setArguments(bundle);

            FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();

            if (addToBackStack)
                fragmentTransaction.addToBackStack(null);

            if (!destFragment.isAdded())
                fragmentTransaction.replace(containerViewId, destFragment, tag).commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideAndShowFragment(FragmentActivity fragmentActivity,
                                       int containerViewId,
                                       Fragment srcFragment,
                                       Fragment destFragment,
                                       Bundle bundle,
                                       String tag,
                                       boolean addToBackStack) {
        try {
            if (destFragment == null)
                return;

            if (bundle != null)
                destFragment.setArguments(bundle);

            FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();

            if (addToBackStack)
                fragmentTransaction.addToBackStack(null);

            if (!destFragment.isAdded()) {
                fragmentTransaction.hide(srcFragment).show(destFragment).commit();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param activity
     * @param viewId
     */
    public static void removeFragmentFromContainer(AppCompatActivity activity, int viewId) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        if (ft != null) {
            ft.remove(activity.getSupportFragmentManager().findFragmentById(viewId));
            ft.commit();
        }
    }

    /**
     * @param activity
     * @param tag
     * @return
     */
    public static Fragment getFragmentByTag(AppCompatActivity activity, String tag) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager != null) {
            Fragment fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment != null)
                return fragment;
        }
        return null;
    }


    /**
     * @param activity
     * @param tag
     */
    public static void removeFragmentByTag(AppCompatActivity activity, String tag) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager != null) {
            Fragment fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment != null)
                fragmentManager.beginTransaction().remove(fragment).commit();
        }

    }

    public static void reloadFragment(AppCompatActivity activity, Fragment fragment) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        if (Build.VERSION.SDK_INT >= 26) {
            ft.setReorderingAllowed(false);
        }
        ft.detach(fragment).attach(fragment).commit();
    }

    public static Fragment getCurrentFragmentIsVisible(AppCompatActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() == 0) {
            return null;
        }
        String tag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        return getFragmentByTag(activity, tag);
    }
}

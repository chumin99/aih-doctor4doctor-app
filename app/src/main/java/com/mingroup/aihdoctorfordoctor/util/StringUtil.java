package com.mingroup.aihdoctorfordoctor.util;

import android.os.Build;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import lombok.SneakyThrows;

public class StringUtil {
    private static final String VIETNAM_COUNTRY_CODE = "(+84) ";
    private static final String VIETNAM_COUNTRY_CODE_2 = "+84";
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    public static String generatePhoneWithCountryCode(String numPhone) {
        String phone = numPhone;
        if (numPhone.startsWith("0", 0)) {
            phone = phone.substring(1);
        }
        return VIETNAM_COUNTRY_CODE + phone.substring(0, 3) + " " + phone.substring(3, 6) + " " + phone.substring(6, 9);
    }

    public static String generatePhoneNumberFromContact(String numPhone) {
        String phone = numPhone.replaceAll("[\\s()-]", "");
        if (phone.startsWith("0", 0)) {
            phone = phone.substring(1);
        }
        return VIETNAM_COUNTRY_CODE_2 + phone;
    }

    public static String generatePhoneNumber(String phoneNumber) {
        StringBuilder strBuilder = new StringBuilder(phoneNumber);
        if (phoneNumber.length() == 9) {
            strBuilder.insert(0, "0");
        }
        return strBuilder.toString();
    }

    // formatted: 21:46, 14/10/2020
    public static String formatDateTime(LocalDateTime localDateTime) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm a, dd/MM/yyyy");
            return localDateTime.format(dateTimeFormatter);
        }
        return localDateTime.toString();
    }

    public static Date convertDatePickerTextToFormattedDate(String datePickerText) throws ParseException {
        datePickerText = datePickerText.replace("thg ", "");
        DateFormat originalDateFormat = new SimpleDateFormat("dd MM, yyyy");
        return originalDateFormat.parse(datePickerText);
    }

    public static String convertDateToString(Date date) {
        DateFormat targetDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return targetDateFormat.format(date);
    }

    public static Date convertStringToDate(String dateStr) throws ParseException {
        DateFormat targetDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return targetDateFormat.parse(dateStr);
    }

    public static Date fromISODateString(String string) throws ParseException {
        if (string != null && !string.isEmpty())
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(string);
        return null;
    }

    public static String toISODateString(Date date) {
        if (date != null)
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(date);
        return null;
    }

    public static String generateLastTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM");
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(new Date());
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);

        if (date.after(new Date())) {
            return null;
        }

        int yearCompareValue = Integer.compare(currentCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.YEAR));
        if (yearCompareValue == 1) {
            return sdf.format(date);
        } else { // cùng năm với năm hiện tại
            int monthGap = currentCalendar.get(Calendar.MONTH) - dateCalendar.get(Calendar.MONTH);
            int dayGap = currentCalendar.get(Calendar.DAY_OF_MONTH) - dateCalendar.get(Calendar.DAY_OF_MONTH);
            if (monthGap == 0) {
                long diff = new Date().getTime() - date.getTime();
                if (dayGap == 0) {
                    int hourGap = currentCalendar.get(Calendar.HOUR_OF_DAY) - dateCalendar.get(Calendar.HOUR_OF_DAY);
                    if (hourGap == 0) {
                        int minutesGap = currentCalendar.get(Calendar.MINUTE) - dateCalendar.get(Calendar.MINUTE);
                        if (minutesGap == 0) {
                            long secondGap = currentCalendar.get(Calendar.SECOND) - dateCalendar.get(Calendar.SECOND);
                            return secondGap + " giây";
                        }
                        return minutesGap + " phút";
                    } else {
                        return hourGap + " giờ";
                    }
                } else if (dayGap < 7) {
                    return dayGap + " ngày";
                } else
                    return sdf1.format(date);
            } else {
                return sdf1.format(date);
            }
        }
    }

    public static String fromDateToString(Date date){
        if(date != null)
            return dateFormat.format(date);
        return "";
    }

    @SneakyThrows
    public static Date fromDateStringToDate(String dateStr){
        if(!dateStr.isEmpty())
            return dateFormat.parse(dateStr);
        return null;
    }

    public static String isMaleOrFemale(boolean isMale){
        return isMale ? "Nam" : "Nữ";
    }

    public static int calculateAge(Date dob) {
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(dob.getTime());

        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        return now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    // formatted: 21:46, 14/10/2020
    public static String formatChatDateTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm a, dd/MM/yyyy");
        return sdf.format(date);
    }

    public static String formatChatTime(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        return sdf.format(date);
    }

    public static String generateRandomFileName() {
        int leftLimit = 48; // numeral '0'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 7;
        Random random = new Random();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd");

        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();

        return generatedString + "-" + sdf.format(new Date());
    }
}

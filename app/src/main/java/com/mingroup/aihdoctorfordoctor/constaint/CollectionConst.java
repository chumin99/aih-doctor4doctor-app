package com.mingroup.aihdoctorfordoctor.constaint;

public class CollectionConst {
    public static final String COLLECTION_DOCTOR = "doctors";
    public static final String COLLECTION_SPECIALTY = "specialites";
    public static final String COLLECTION_QUESTION = "questions";
    public static final String COLLECTION_REPLY = "replies";
    public static final String COLLECTION_CONSERVATION = "conservations";
    public static final String COLLECTION_USER = "users";
}

package com.mingroup.aihdoctorfordoctor.constaint;

public class StorageConst {
    public static final String AVATAR_FOLDER = "images_avatar";
    public static final String QUESTION_FOLDER = "images_question";
    public static final String CHAT_FOLDER = "images_chat";
}
